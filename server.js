'use strict';

const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const http = require('http');
const cors = require('cors');
const mongoose = require('mongoose');
const helmet = require('helmet');
const cloudinary = require('cloudinary');

const routes = require('./server/routes');
require('babel-register');
const app = express();
const router = express.Router();

// for local
// const url = process.env.MONGODB_URI || 'mongodb://localhost:27017/greenfield';
// for remote
const url =
  'mongodb://admin:Chettiet123456@ds245240.mlab.com:45240/greenfielddb';
const port = process.env.PORT || 4200;

// config cloudinary
cloudinary.config({
  cloud_name: 'YOUR_CLOUDINARY_NAME_HERE',
  api_key: 'YOUR_CLOUDINARY_API_KEY_HERE',
  api_secret: 'YOUR_CLOUDINARY_API_SECRET_HERE'
});

// connect to MongoDB datastore
try {
  mongoose.connect(url);
  const db = mongoose.connection;
  db.on('error', console.error.bind(console, 'Database connection error:'));
  db.once('open', function() {
    console.log('Connect database successfully!');
  });
} catch (e) {
  console.log(e);
}

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());

// CHANGE LATER
// // catch 404 and forward to error handler
// app.use((req, res, next) => {
//   var err = new Error('Not Found');
//   err.status = 404;
//   next(err);
// });

// // error handler
// app.use((err, req, res, next) => {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};

//   // render the error page
//   res.status(err.status || 500);
//   res.render('error');
// });

/** set up routes {API Endpoints} */
routes(router);
app.use('/api/v1', router);

// use for load static files
app.use(express.static(path.join(__dirname, 'build')));

// rewrite virtual urls to frontend app to enable refreshing of internal pages
app.get('*', function(req, res, next) {
  res.sendFile(path.resolve(path.join(__dirname, 'build/index.html')));
});

const server = http.createServer(app);
server.listen(port, () => console.log(`Running on localhost:${port}`));
