const Provider = require('../models/Provider');
const User = require('../models/User');
const Role = require('../models/Role');
const bcrypt = require('bcryptjs');

module.exports = {
  create: (req, res, next) => {
    const { shopName, phone, email, shippingPrice, address } = req.body;
    const createdDate = new Date();
    const isDeleted = false;
    const certificates = [];
    const rating = 0;

    saveProvider({ shopName, phone, email, shippingPrice, address, createdDate, isDeleted, certificates, rating });

    function saveProvider(obj) {
      new Provider(obj).save((err, item) => {
        if (err) res.send(err);
        else if (!item) res.sendStatus(400);
        else {
          Role.findOne({ code: 2 }).exec((err, _role) => {
            const hashedPassword = bcrypt.hashSync(obj.phone, 8);
            saveUser({
              phone: obj.phone,
              password: hashedPassword,
              fullname: obj.shopName,
              email: obj.email,
              address: obj.address,
              isActived: true,
              isDeleted: false,
              createdDate: new Date(),
              role: _role
            }, () => {
              res.status(200).send(item);
            });
          });
        }
      })
    }

    function saveUser(obj, cb) {
      new User(obj).save((err, item) => {
        if (err) res.send(err);
        else if (!item) res.sendStatus(400);
        else {
          cb();
        }
      })
    }
  },
  getAll: (req, res, next) => {
    Provider.find()
      .exec((err, items) => {
      if (err) res.send(err);
      else res.send(items);
    });
  },
  getById: (req, res, next) => {
    const _id = req.params.id;
    Provider.findById(_id).exec((err, item, next) => {
      if (err) res.send(err);
      else if (!item) res.sendStatus(404);
      else res.send(item);
    });
  }
}