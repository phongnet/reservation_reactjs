/**
 * server/controllers/demo.controller.js
 * Just for demo and testing
*/

module.exports = {
    create: (req, res, next) => {
        let { name } = req.body;
        res.send({
          id: (new Date()).getTime(),
          name: name
        });
        // next();
    },
    getAll: (req, res, next) => {
      res.send([
        {id: 1, name: 'name 1'},
        {id: 2, name: 'name 2'},
        {id: 3, name: 'name 3'}
      ]);
      // next();
    },
    getById: (req, res, next) => {
      const _id = req.params.id;
      res.send({
        id: _id,
        name: 'name ' + _id
      });
      // next();
    }
}