const Role = require('../models/Role');

module.exports = {
  create: (req, res, next) => {
    const { code, name, description } = req.body;

    saveRole({ code, name, description });

    function saveRole(obj) {
      new Role(obj).save((err, item) => {
        if (err) res.send(err);
        else if (!item) res.sendStatus(400);
        else res.send(item);
      });
    }
  },
  getAll: (req, res, next) => {
    Role.find((err, items) => {
      if (err) res.send(err);
      else res.send(items);
    });
  },
  getById: (req, res, next) => {
    const _id = req.params.id;
    Role.findById(_id).exec((err, item, next) => {
      if (err) res.send(err);
      else if (!item) res.sendStatus(404);
      else res.send(item);
    });
  },
  getByCode: (req, res, next) => {
    const _code = req.params.code;
    Role.findOne({ code: _code }).exec((err, item, next) => {
      if (err) res.send(err);
      else if (!item) res.sendStatus(404);
      else res.send(item);
    });
  }
};
