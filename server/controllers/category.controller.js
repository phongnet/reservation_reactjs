const Category = require('../models/Category');

module.exports = {
  create: (req, res, next) => {
    const { code, name, description, imageUrl } = req.body;

    saveCategory({ code, name, description, imageUrl });

    function saveCategory(obj) {
      new Category(obj).save((err, item) => {
        if (err) res.send(err);
        else if (!item) res.sendStatus(400);
        else res.send(item);
      });
    }
  },
  getAll: (req, res, next) => {
    Category.find((err, items) => {
      if (err) res.send(err);
      else res.send(items);
    });
  },
  getById: (req, res, next) => {
    const _id = req.params.id;
    Category.findById(_id).exec((err, item, next) => {
      if (err) res.send(err);
      else if (!item) res.sendStatus(404);
      else res.send(item);
    });
  },
  getByCode: (req, res, next) => {
    const _code = req.params.code;
    Category.findOne({ code: _code }).exec((err, item, next) => {
      if (err) res.send(err);
      else if (!item) res.sendStatus(404);
      else res.send(item);
    });
  }
};
