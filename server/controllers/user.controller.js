const User = require('../models/User');
const Role = require('../models/Role');

module.exports = {
  create: (req, res, next) => {
    const { phone, password, fullname, email, address } = req.body;
    const createdDate = new Date();
    const isActived = true;
    const isDeleted = false;

    saveUser({ phone, password, fullname, email, address, createdDate, isActived, isDeleted });

    function saveUser(obj) {
      new User(obj).save((err, user) => {
        if (err) res.send(err);
        else if (!user) res.sendStatus(400);
        else res.send(user);
      })
    }
  },
  getAll: (req, res, next) => {
    User.find()
      .populate("role")
      .exec((err, users) => {
      if (err) res.send(err);
      else res.send(users);
    });
  },
  getById: (req, res, next) => {
    const _id = req.params.id;
    User.findById(_id).exec((err, user, next) => {
      if (err) res.send(err);
      else if (!user) res.sendStatus(404);
      else res.send(user);
    });
  }
}