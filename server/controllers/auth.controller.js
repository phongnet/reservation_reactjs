var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var serverConfig = require('../config/server.config');
const User = require('../models/User');
const Role = require('../models/Role');

export const register = (req, res, next) => {
  Role.findOne({ code: 1 }).exec((err, _role) => {
    if (err) res.send(err);
    else if (!_role) res.sendStatus(404);
    else {
      const hashedPassword = bcrypt.hashSync(req.body.password, 8);
      _saveUser({
        phone: req.body.phone,
        password: hashedPassword,
        fullname: req.body.fullname,
        email: req.body.email,
        address: req.body.address,
        role: _role
      });
    }
  });

  function _saveUser (obj) {
    User.create(obj, (err, _user) => {
        if (err)
          return res
            .status(500)
            .send('There was a problem registering the user.');
        // create a token
        const token = jwt.sign({ id: _user._id }, serverConfig.secret, {
          expiresIn: 86400
        });
        res.status(200).send({
          token: token,
          user: {
            id: _user._id,
            phone: _user.phone,
            role: obj.role
          }
        });
      }
    );
  }
};

export const login = (req, res) => {
  // TODO: hard code for user has role admin & provider
  if (req.body.phone === 'admin' && req.body.password === '123456') {
    var token = jwt.sign({ id: '3' }, serverConfig.secret, {
      expiresIn: 86400
    });
    res.status(200).send({
      token: token,
      user: {
        id: '3',
        phone: "admin",
        role: {
          code: 3
        }
      }
    });
    return;
  }

  if (req.body.phone === 'provider' && req.body.password === '123456') {
    var token = jwt.sign({ id: '2' }, serverConfig.secret, {
      expiresIn: 86400
    });
    res.status(200).send({
      token: token,
      user: {
        id: '2',
        phone: "provider",
        role: {
          code: 2
        }
      }
    });
    return;
  }

  // check login for user has role customer
  User.findOne({ phone: req.body.phone }).populate("role").exec((err, _user) => {
    if (err) return res.status(500).send('Error on the server.');
    if (!_user) return res.status(401).send('No user found.');
    var passwordIsValid = bcrypt.compareSync(req.body.password, _user.password);
    if (!passwordIsValid)
      return res.status(401).send({ auth: false, token: null });
    var token = jwt.sign({ id: _user._id }, serverConfig.secret, {
      expiresIn: 86400
    });
    res.status(200).send({
      token: token,
      user: {
        id: _user._id,
        phone: _user.phone,
        role: _user.role
      }
    });
  });
};
