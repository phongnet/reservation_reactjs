// server/models/Product.js
const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema(
  {
    code: String,
    name: String,
    price: Number,
    discount: Number,
    unit: String,
    quantity: Number,
    description: String,
    imageUrl: String,
    subImageUrls: Array,
    featured: Boolean,
    isActived: Boolean,
    isDeleted: Boolean,
    createdDate: Date,
    category: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Category'
    },
    provider: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Provider'
    }
  }
);
module.exports = mongoose.model('Product', ProductSchema);