// server/models/Provider.js
const mongoose = require('mongoose');

const ProviderSchema = new mongoose.Schema(
  {
    shopName: String,
    phone: String,
    email: String,
    shippingPrice: Number,
    address: String,
    rating: Number,
    certificates: Array,
    isDeleted: Boolean,
    createdDate: Date
  }
);
module.exports = mongoose.model('Provider', ProviderSchema);