// server/models/User.js
const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema(
    {
        phone: String,
        password: String,
        fullname: String,
        email: String,
        address: String,
        isActived: Boolean,
        isDeleted: Boolean,
        createdDate: Date,
        role: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Role'
        }
    }
);
module.exports = mongoose.model('User', UserSchema);