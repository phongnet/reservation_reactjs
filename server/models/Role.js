// server/models/Role.js
const mongoose = require('mongoose');

const RoleSchema = new mongoose.Schema(
    {
        code: Number,
        name: String,
        description: String
    }
);
module.exports = mongoose.model('Role', RoleSchema);