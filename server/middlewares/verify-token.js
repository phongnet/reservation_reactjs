var jwt = require('jsonwebtoken');
var serverConfig = require('../config/server.config');

module.exports = (req, res, next) => {
  var token = req.headers['x-access-token'];
  if (!token) return res.status(403).send({ message: 'No token provided.' });
  jwt.verify(token, serverConfig.secret, function(err, decoded) {
    if (err)
      return res.status(500).send({ message: 'Failed to authenticate token.' });
    // if everything good, save to request for use in other routes
    req.userId = decoded.id;
    next();
  });
};
