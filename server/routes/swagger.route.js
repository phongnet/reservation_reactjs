const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../swagger/swagger.json'); // user for locally

// setup openAPI swagger
const options = {
  /** user specification in swagger.io */
  // swaggerUrl: 'http://petstore.swagger.io/v2/swagger.json'
};

module.exports = (router) => {
  router.use('/', swaggerUi.serve, swaggerUi.setup(swaggerDocument, options));
};