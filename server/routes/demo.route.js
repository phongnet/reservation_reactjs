const multipart = require('connect-multiparty');
const multipartWare = multipart();
const demoController = require('../controllers/demo.controller');

module.exports = (router) => {
    /**
     * get all objects
     */
    router.route('/objects').get(demoController.getAll);
    /**
     * add an object
     */
    router.route('/objects').post(multipartWare, demoController.create);
    /**
     * get object by id
     */
    router.route('/objects/:id').get(demoController.getById);
}