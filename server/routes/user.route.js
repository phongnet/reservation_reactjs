const multipart = require('connect-multiparty');
const multipartWare = multipart();
const userController = require('../controllers/user.controller');

module.exports = (router) => {

    router.route('/users').get(userController.getAll);
    
    router.route('/users').post(multipartWare, userController.create);
    
    router.route('/users/:id').get(userController.getById);
};