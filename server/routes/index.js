// parent routing
const demoRoute = require('./demo.route');
const swaggerRoute = require('./swagger.route');
const userRoute = require('./user.route');
const authRouter = require('./auth.route');
const categoryRoute = require('./category.route');
const providerRoute = require('./provider.route');
const productRoute = require('./product.route');

module.exports = router => {
  demoRoute(router); // Just for demo
  userRoute(router); // APIs for user module
  categoryRoute(router); // APIs for category module
  authRouter(router); // route for authentication
  providerRoute(router); // route for provider
  productRoute(router); // route for product
  swaggerRoute(router); // route for swagger
};
