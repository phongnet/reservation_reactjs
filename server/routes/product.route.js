const multipart = require('connect-multiparty');
const multipartWare = multipart();
const productController = require('../controllers/product.controller');
const verifyToken = require('../middlewares/verify-token');

module.exports = router => {
  router.route('/products').get(productController.getAll);

  router.route('/products').post(productController.create);

  router.route('/products/:id').get(productController.getById);

  router.route('/products?code=').get(productController.getByCode);

  router.route('/products/providerId=').get(productController.getByProviderId);
};
