const authController = require('../controllers/auth.controller');

module.exports = router => {
  router.route('/login').post(authController.login);
  router.route('/register').post(authController.register);
};
