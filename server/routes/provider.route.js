const multipart = require('connect-multiparty');
const multipartWare = multipart();
const providerController = require('../controllers/provider.controller');

module.exports = (router) => {

    router.route('/providers').get(providerController.getAll);
    
    router.route('/providers').post(multipartWare, providerController.create);
    
    router.route('/providers/:id').get(providerController.getById);
};