const multipart = require('connect-multiparty');
const multipartWare = multipart();
const categoryController = require('../controllers/category.controller');
const verifyToken = require('../middlewares/verify-token');

module.exports = router => {
  router.route('/categories').get(categoryController.getAll);

  router.route('/categories').post(verifyToken, categoryController.create);

  router.route('/categories/:code').get(categoryController.getByCode);
};
