import React from "react";

export default function MarkerSimple () {
  return (
    <i className="fa fa-thumb-tack" aria-hidden="true" style={{ fontSize: 30, color: '#8BC34A' }}></i>
  )
}