import React, { Component } from 'react';
import { connect } from 'react-redux';
import ItemImg from "../../../assets/images/cu-sach.jpg";
import './ProductItem.css';
import { TOGGLE_PRODUCT_DETAIL } from "../../../actions/product.action";

class ProductItem extends Component {
  render() {
    const { product } = this.props;
    return (
      <div className="ProductItem" onClick={this.props.showProdDetail.bind(this, Object.assign({}, product))}>
        <div className="ProductItemCover">
            <div className="ProductItemImg">
                <img src= {ItemImg}/>
            </div>
            <span className="ProductTitle">{product.name}</span>
            <div className="ProductPriceCover">
                <div className="MainPrice">
                    <span>{product.price}d</span>
                    <span>{product.price - (product.price * product.discount)}d</span>
                </div>
                {
                  product.isFreeShip && <span className="ProductShippingIcon"><i className="fa fa-truck" aria-hidden ="true"></i></span>
                }
            </div>
            <div className="ProductRating">
                <div className="ProductRatingLeft">
                    <span><i className="fa fa-heart-o"></i></span>
                    <span>1999</span>
                </div>
                <div className="ProductRatingRight">
                    <div className="StartRating">
                        <span className="fa fa-star checked"></span>
                        <span className="fa fa-star checked"></span>
                        <span className="fa fa-star checked"></span>
                        <span className="fa fa-star"></span>
                        <span className="fa fa-star"></span>
                    </div>
                    <span>(2000)</span>
                </div>
            </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    state
  };
};
const mapDispatchToProps = dispatch => {
  return {
    showProdDetail: (product) => dispatch({ type: TOGGLE_PRODUCT_DETAIL, toggleProdDetail: true, product})
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductItem);
