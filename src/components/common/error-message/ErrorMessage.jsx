import React from 'react';
import './ErrorMessage.css';

export default function ({content}) {
  return (
    <div className="ErrorMessage">
      <span>{content}</span>
    </div>
  );
}