import React from 'react';
import './TinyLoading.css';

export default function ({size}) {
  return (
    <div className="TinyLoading">
      <i className="fa fa-refresh fa-spin" style={{fontSize: size || 30}}></i>
    </div>
  );
}