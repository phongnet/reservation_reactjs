import React, { Component } from 'react';
import { connect } from 'react-redux';
import './MainContent.css';

class MainContent extends Component {
  render() {
    return (
      <div className="MainContent">
        <span>Main content day roi</span>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    state
  };
};
const mapDispatchToProps = dispatch => {
  return {
    dispatch
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MainContent);
