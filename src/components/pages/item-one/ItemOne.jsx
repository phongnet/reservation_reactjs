import React, { Component } from 'react';
import { connect } from 'react-redux';
import './ItemOne.css';

class ItemOne extends Component {
  render() {
    return (
      <div className="ItemOne">
        Item one
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    state
  };
};
const mapDispatchToProps = dispatch => {
  return {
    dispatch
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemOne);
