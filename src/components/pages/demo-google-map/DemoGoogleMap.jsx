import React, { Component } from 'react';
import { connect } from 'react-redux';
import './DemoGoogleMap.css';
import GoogleMapReact from 'google-map-react';

const AnyReactComponent = ({ text }) => <div style={{ fontSize: 20 }}>{text}</div>;

class DemoGoogleMap extends Component {
    static defaultProps = {
        center: { lat: 40.7446790, lng: -73.9485420 },
        zoom: 11
    }
    render() {
        return (
            // Important! Always set the container height explicitly
            <div style={{ display: 'flex', flex: 1, width: '100%', height: '100vh' }}>
                <GoogleMapReact
                    bootstrapURLKeys={{ key: "AIzaSyCP7oMhboMLwI0AK7hUeJIqe25HgrjUWHY" }}
                    defaultCenter={this.props.center}
                    defaultZoom={this.props.zoom}
                >
                    <AnyReactComponent
                        lat={40.7446790}
                        lng={-73.9485420}
                        text={'ADDRESS'}
                    />
                </GoogleMapReact>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        state
    }
};
const mapDispatchToProps = dispatch => {
    return {
        dispatch
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(DemoGoogleMap);
