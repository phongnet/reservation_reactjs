import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import MainApp from './main/MainApp';
import './App.css';
import LoginPopup from '../layout/login-popup/LoginPopup';
import DemoGoogleMap from '../pages/demo-google-map/DemoGoogleMap';
import ProductDetailPopup from '../layout/product-detail-popup/ProductDetailPopup';


class App extends Component {
  render() {
    return (
      <div className="App">
        <Switch>
          <Route path="/" component={MainApp} />
          <Route path="/home" component={MainApp} />
          <Route path="/main-content" component={MainApp} />
          <Route path="/itemone" component={MainApp} />
          <Route path="/demo-google-map" component={DemoGoogleMap} />
          <Route render={() => <div className="NotFoundMessage">404 Page Not Found</div>} />
        </Switch>
        <LoginPopup />
        <ProductDetailPopup/>
      </div>
    );
  }
}

export default App;
