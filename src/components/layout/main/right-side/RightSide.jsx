import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import './RightSide.css';
import Header from '../header/Header';
import Content from '../content/Content';
import Footer from '../footer/Footer';

class RightSide extends Component {
  render() {
    return (
      <div className="RightSide">
          <Header/>
          <Content {...this.props}/>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    state
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RightSide);
