import React, { Component } from 'react';
import { connect } from 'react-redux';
import './MainApp.css';
import LeftSide from './left-side/LeftSide';
import RightSide from './right-side/RightSide'
class MainApp extends Component {
  render() {
    return (
      <div className="MainApp">
        <div className="MainAppWrapper">
          <LeftSide {...this.props}/>
          <RightSide {...this.props}/>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    state
  };
};
const mapDispatchToProps = dispatch => {
  return {
    dispatch
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MainApp);