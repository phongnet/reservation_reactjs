import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import AppConfig from '../../../../config'
import './LeftSide.css';

class LeftSide extends Component {
    constructor(props) {
        super(props);
        this.state = {
            menus: [
                { id: 1, title: "Dashboard", to: "/home", icon: "fa fa-home", isActive: true },
                { id: 2, title: "Content", to: "/main-content", icon: "fa fa-file-text" },
                { id: 3, title: "Item One", to: "/itemone", icon: "fa fa-area-chart" },
                { id: 4, title: "Products", to: "/product-list", icon: "fa fa-bug" }
            ]
        };
    }
    render() {
        const { menus } = this.state;
        return (
            <div className="LeftSide">
                <div className="AppNameLeft">
                    <span className="AppTitle"><Link to ="/home">{AppConfig.appName}</Link></span>
                </div>
                <div className="LeftSideMenu">
                    {
                        menus.map((item, index) => {
                            return (
                                <Link key={index} to={item.to} onClick={() => this.activeItem(item.id)}>
                                    <div className={`MenuItem ${item.isActive ? 'actived' : ''}`}>
                                        <i className={item.icon} aria-hidden="true"></i>
                                        <span>{item.title}</span>
                                    </div>
                                </Link>
                            )
                        })
                    }
                </div>
            </div>
        );
    }

    componentDidMount() {
        const { pathname } = this.props.location;
        this.setState({
            menus: this.state.menus.map(item => {
                item.isActive = (item.to === pathname)
                return item
            })
        });
    }

    activeItem(_id) {
        this.setState({
            menus: this.state.menus.map(item => {
                item.isActive = (item.id === _id)
                return item
            })
        }
        );
    }
}

const mapStateToProps = state => {
    return {
        state
    };
};

const mapDispatchToProps = dispatch => {
    return {
        dispatch
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(LeftSide);
