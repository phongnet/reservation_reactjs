import React, { Component } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Home from "../../../pages/home/Home";  
import ItemOne from "../../../pages/item-one/ItemOne";
import MainContent from "../../../pages/main-content/MainContent";
import ProductList from "../../../pages/product-list/ProductList";
import "./Content.css";

class Content extends Component {
  render() {
    const { match } = this.props;
    return (
      <div className="Content">
        <Switch>
          <Route exact = {true} path="/" component={Home} />
          <Route path= "/home" component={Home} />
          <Route path = "/main-content" component = {MainContent} />
          <Route path="/itemone" component = {ItemOne}/>
          <Route path="/product-list" component = {ProductList}/>
          <Route render={() => <div className="NotFoundMessage">404 Page Not Found</div>} />
        </Switch>
      </div>
    );
  }

  componentDidMount () {
    // Test jQuery here
  }
}

export default Content;