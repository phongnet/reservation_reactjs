import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import "./Header.css";
import AppConfig from "../../../../config";
import {TOGGLE_LOGIN} from "../../../../actions/login.action";
import SessionHelper from "../../../../helpers/session.helper";

class Header extends Component {
  constructor () {
    super();
    const _loggedInUser = SessionHelper.getUser();
    this.state = {
      isLoggedIn: _loggedInUser !== null,
      loggedInUser: _loggedInUser
    };
  }
  render() {
    return (
      <div className="Header">
          {/*<span className="HeaderTitle"><Link to={"/home"}>{AppConfig.appName}</Link></span>*/}
          {
            this.state.isLoggedIn === false ? (
              <div className="HeaderRight">
                <span onClick={this.props.showLogin.bind(this)}>Login</span>
                <span onClick={this.props.showRegister.bind(this)}>Register</span>
              </div>
            ) : (
              <div className="HeaderRight">
                <span><Link to={"/home/admin"}>{this.state.loggedInUser.phone}</Link></span>
                <span onClick={this.logout.bind(this)}>Logout</span>
              </div>
            )
          }
      </div>
    );
  }

  logout () {
    SessionHelper.clear();
    window.location.href = "/";
  }
}

const mapStateToProps = state => {
  return {
    state
  }
};

const mapDispatchToProps = dispatch => {
  return {
    showLogin: () => {
      dispatch({type: TOGGLE_LOGIN, toggleLogin: true, showLoginTab: true})
    },
    showRegister: () => {
      dispatch({type: TOGGLE_LOGIN, toggleLogin: true, showRegisterTab: true})
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);